import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

When("Я вижу, что выбрана вкладка {string}",(tabName) => {
    cy.get(`li:contains("${tabName}")`).should('have.class', 'active')
})

When("Я заполняю текстовое поле {string} значением {string}",(fieldName, text) => {
    cy.get(`label:contains("${fieldName}")`).parent().find('input').type(text)
})

When("Я заполняю большое текстовое поле {string} значением {string}",(fieldName, text) => {
    cy.get(`label:contains("${fieldName}")`).parent().find('textarea').type(text)
})

When("Я выбираю преполагаемую карту {string}", (cardNum) => {
    cy.get(`a[ng-click="estimate(card)"]:contains("${cardNum}")`).click()
})

When("Я вижу выбранную предполагаемую карту {string}", (cardNum) => {
    cy.get('p.text-right').find(`span:contains("${cardNum}")`).should('be.visible')
})

When("Я добавляю структурированную стори", () => {
    cy.get('form[name="formStructured"]>button:contains("Add story")').click()
})
