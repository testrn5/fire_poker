import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps"

When("Я заполняю текстовое поле {string} значением {string}",(fieldName, text) => {
    cy.get(`label:contains("${fieldName}")`).parent().find('input').type(text)
})

When("Я заполняю большое текстовое поле {string} значением {string}",(fieldName, text) => {
    cy.get(`label:contains("${fieldName}")`).parent().find('textarea').type(text)
})

When("Я выбираю из списка {string} значение {string}",(selectName, text) => {
    cy.get(`label:contains("${selectName}")`).parent().find('select').select(text)
})
